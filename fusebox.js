const { FuseBox } = require('fuse-box');

const fuse = FuseBox.init({
  homeDir: 'src',
  target: 'server@esnext',
  output: 'dist/$name.js',
});

fuse
  .bundle('server@es2017')
  .instructions(' > [app/index.ts]')
  .watch('src/app/**')

  .completed(proc => proc.start());

fuse.run();
