import { Server } from 'hapi';

import { initAssoiciations } from 'app/sources/associations';
import { initSources } from 'app/sources';

const server = new Server({
  port: 3000,
  host: 'localhost',
});

initAssoiciations();
initSources(server);

const init = async () => {
  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', err => {
  console.log(err);
  server.stop();
});

init();
