import { initTests } from './tests';
import { initGroups } from './groups';
import { initDevelopment } from './development';

export const initSources = server => {
  initDevelopment(server);
  initGroups(server);
  initTests(server);
};
